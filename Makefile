
BIN = zqpool
SOURCES = src/main.go src/server.go src/config.go

all:
	go build  -o $(BIN) $(SOURCES)
