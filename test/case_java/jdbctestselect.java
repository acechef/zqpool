import java.sql.*;
import java.io.*;
import java.util.*;

class jdbctestselect extends Thread {

	static public void main(String args[]) {

		if (args.length != 1) {
			System.out.println("Example:java jdbctestselect <type>\n");
			return;
		}


		// "jdbc:oracle:thin:@localhost:1521:boss"
		// "jdbc:postgresql://127.0.0.1:5432/test"
		String dburl = "jdbc:postgresql://127.0.0.1:5436/postgres";
		String username = "u01";
		String password = "u01";

		int runType = Integer.parseInt(args[0]);

		int runCnt = 10;

		System.out.println("RunType=" + String.valueOf(runType) + ", Start...");
		Connection conn = null;
		try {
			// 第一步，建立驱动程序实例
			// Class.forName("oracle.jdbc.driver.OracleDriver");
			Class.forName("org.postgresql.Driver");

			// 第二步，获得连接
			conn = DriverManager.getConnection(dburl, username, password);
			//conn.setAutoCommit(false);
			conn.setAutoCommit(true);
			//
			if (runType == 1) {
				ResultSet rs = null;
				Statement statement = conn.createStatement();
				String runsql = "select id, name from person where id=1";
				rs = statement.executeQuery(runsql);
				if (rs.next()) {
					System.out.println(rs.getString(1)+"\t"+rs.getString(2));
				}
				rs.close();
				rs = null;
			} else if (runType == 2) {
				String runsql = "select id, name from person where id=?";
				ResultSet rs = null;
				PreparedStatement pstmt = null;
				pstmt = conn.prepareStatement(runsql);
				for (int i = 0; i < runCnt; i++) {
					pstmt.setInt(1, i);
					rs = pstmt.executeQuery();
					if (rs.next()) {
						System.out.println(rs.getString(1)+"\t"+rs.getString(2));
					}
					rs.close();
					rs = null;
				}
				pstmt.close();
				pstmt = null;
			} else if (runType == 3) {
				String runsql1 = "select name from person where id=?";
				String runsql2 = "select id from person where name=?";
				ResultSet rs1 = null;
				ResultSet rs2 = null;
				PreparedStatement pstmt1 = conn.prepareStatement(runsql1);
				PreparedStatement pstmt2 = conn.prepareStatement(runsql2);

				for (int i = 0; i < runCnt; i++) {
					pstmt1.setInt(1, i);
					rs1 = pstmt1.executeQuery();
					if (rs1.next()) {
						//System.out.println(rs1.getString(1)+"\t"+rs1.getString(2));
						System.out.println(rs1.getString(1));
					}

					pstmt2.setString(1, String.valueOf(i)+"abcdefghijk");;
					rs2 = pstmt2.executeQuery();
					if (rs2.next()) {
						//System.out.println(rs1.getString(1)+"\t"+rs1.getString(2));
						System.out.println(rs1.getString(1));
					}

					rs1.close();
					rs1 = null;
				}
				pstmt1.close();
				pstmt1 = null;
				pstmt2.close();
				pstmt2 = null;
			}
			conn.close();
			conn = null;
		} catch (java.sql.SQLException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
