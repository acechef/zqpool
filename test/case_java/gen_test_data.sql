CREATE TABLE person (
  id bigserial NOT NULL,
  name varchar(255),
  PRIMARY KEY (id)
);

INSERT INTO person(name) select seq||'abcdefghijk' FROM generate_series(1,10) as seq;
