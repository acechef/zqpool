package main

import (
	"flag"
	"fmt"

	//"os"
	"runtime"
	//"strings"
	"log"
	"time"
)

var g_proxyConf = make(map[string]string, 4)
var g_backends = make(map[string]*DbConfig)

func main() {
	var debug = flag.Bool("debug", false, "Enable debug mode")
	var configFile = flag.String("conf", "zqpool.conf", "Specify configuration file name")
	//var backend = flag.String("backend", "", "Backend Database servers")
	flag.Parse()
	log.SetFlags(log.Lshortfile | log.LstdFlags)

	if *debug {
		go debugRoutine()
	}

	err := LoadConfig(*configFile, g_proxyConf, g_backends)
	if err != nil {
		log.Printf("Load config file %s failed: %s\n", *configFile, err.Error())
		return
	}

	var listenIp string
	if g_proxyConf["listen_addr"] == "*" {
		listenIp = ":" + g_proxyConf["listen_port"]
	} else {
		listenIp = g_proxyConf["listen_addr"] + ":" + g_proxyConf["listen_port"]
	}
	startServer(listenIp)
}

func debugRoutine() {
	for {
		<-time.After(2 * time.Second)
		fmt.Println(time.Now(), "NumGoroutine", runtime.NumGoroutine())
	}
}
